FROM 32bit/ubuntu:14.04

MAINTAINER Spring Street Securities richard@pinkgrass.org

ENV DEBIAN_FRONTEND noninteractive

# make sure the package repository is up to date and update ubuntu
RUN \
  sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
#  apt-get -y upgrade && \
  locale-gen en_US.UTF-8

# if update then clean packages

RUN apt-get install -y curl git htop man software-properties-common unzip vim wget

RUN apt-get update && apt-get install -y \
    ntp \
    python-pip \
    python-dev \
    python-numpy \
    python-scipy \
    python-matplotlib

# Force clock sync and change to UTC
RUN ntpdate ntp.ubuntu.com
RUN ln -sf /usr/share/zoneinfo/UTC /etc/localtime

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV HOME /root

# supervisor installation && 
# create directory for child images to store configuration in
RUN apt-get -y install supervisor && \
  mkdir -p /var/log/supervisor && \
  mkdir -p /etc/supervisor/conf.d

# supervisor base configuration
ADD supervisord/supervisor.conf /etc/supervisor.conf

# App installation & setup
RUN mkdir -p /usr/src/app && \
    mkdir -p /var/log/app

COPY . /usr/src/app/
WORKDIR /usr/src/app
#RUN pip install --no-cache-dir -r requirements.txt
RUN pip install -r requirements.txt

EXPOSE 8080
EXPOSE 9001

# default command
CMD ["supervisord", "-c", "/etc/supervisor.conf"]

