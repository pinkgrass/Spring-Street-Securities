import click

# import broker


@click.group()
@click.option('--broker', default='oanda', show_default=True, help='set broker')
def main(broker):
    pass


@click.command()
@click.option('--marketdatadir', default='./marketdata/', show_default=True, help='market data directory')
@click.option('--strategydir', default='./startegy/', show_default=True, help='strategy directory')
@click.option('--reportoutput', default='./backtest_report.html', show_default=True, help='backtest report file')
@click.option('--configfileoutput', default='./Spring-Street-Securities.config', show_default=True, help='config file')
def backtest(marketdatadir, strategydir, reportoutput, configfileoutput):
    click.echo('Back test started')
    # Load market history from marketdatadir
    # Load strategies from strategydir
    # Iterate through instrument market history and strategies with parameters
    # Save report
    # Save config file
    click.echo('Back test finished')


@click.command()
@click.option('--apikey', required=True, help='broker apikey')
@click.option('--account', required=True, help='broker account')
@click.option('--marketdatadir', default='./marketdata/', show_default=True, help='market data directory')
def marketdata(apikey, account, marketdatadir):
    click.echo('Market data download started')
    # Initialise Broker
    # Retrieve Instrument list
    # Retrieve market history for each instrument
    # Save market history to directory
    click.echo('Market data download finished')


@click.command()
@click.option('--apikey', required=True, help='broker apikey')
@click.option('--account', required=True, help='broker account')
@click.option('--configfile', default='./Spring-Street-Securities.config', show_default=True, help='config file')
def trader(apikey, account, configfile):
    click.echo('Trader started')
    # Initialise Broker
    # Load config file
    # Load strategies
    # Iterate through config file : initialise feeds
    # Iterate through config file : start strategies for each startegy/feed pair
    click.echo('Trader finished')


main.add_command(backtest)
main.add_command(marketdata)
main.add_command(trader)
