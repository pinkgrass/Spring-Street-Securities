# Spring Street Securities - Trader
#
#   Copyright 2015 Richard Crook
#
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


import os
from oanda import barfeed
from oanda import broker

from strategy import sma

from pyalgotrade.bar import Frequency


# Collect environment variables
DEBUG = bool(os.environ.get('DEBUG','True'))
OANDA_ENVIRONMENT = os.environ.get('OANDA_ENVIRONMENT','practice')
OANDA_ACCESS_TOKEN = os.environ.get('OANDA_ACCESS_TOKEN','666b4dff976034e9b3e9102b31a5dc25-5266906d5d61bc6e3a0d817deee4b534')
OANDA_ACCOUNT = os.environ.get('OANDA_ENVIRONMENT','8638902')

# pass as parameters
STRATEGY = os.environ.get('STRATEGY','strategy/sma.py')
instruments = ['GBP_USD']
frequency = Frequency.MINUTE
parameters = {}

# class threads

def main():
    instruments = ['GBP_USD']
    oanda_feed = barfeed.LiveFeed(OANDA_ENVIRONMENT, OANDA_ACCESS_TOKEN, instruments, frequency)
    oanda_broker = broker.LiveBroker(OANDA_ENVIRONMENT, OANDA_ACCESS_TOKEN, OANDA_ACCOUNT)
    
    trading_strategy = sma.SMA(oanda_feed, oanda_broker, instruments[0])

    trading_strategy.run()

if __name__ == "__main__":
    main()
