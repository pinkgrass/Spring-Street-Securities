import oandapy

environment='practice'
access_token='666b4dff976034e9b3e9102b31a5dc25-5266906d5d61bc6e3a0d817deee4b534'
account='8638902'
instruments = ['EUR_USD','GBP_USD']


oanda = oandapy.API(environment, access_token)

response = oanda.get_history(
    instrument='EUR_USD',
    count=2,
    granularity='H1',
    candleFormat='midpoint')

print(response)
