# PyAlgoTrade
#
# Copyright 2011-2015 Gabriel Martin Becedillas Ruiz
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
.. moduleauthor:: Gabriel Martin Becedillas Ruiz <gabriel.becedillas@gmail.com>
.. moduleauthor:: Richard Crook <richard@pinkgrass.org>
"""

import time
import datetime
import threading
import Queue
import csv

from pyalgotrade.barfeed import csvfeed
from pyalgotrade import bar
from pyalgotrade import barfeed
from pyalgotrade import dataseries
from pyalgotrade import resamplebase
import pyalgotrade.logger
from pyalgotrade.utils import dt
#import api
import oandapy

logger = pyalgotrade.logger.getLogger("oanda-feed")


OANDA_MAX_COUNT = 5000

# Map Oanda frequency to PyAlgoTrade frequency
OANDA_FREQUENCY = {
            bar.Frequency.TRADE: 'all', # The bar represents a single trade.
            bar.Frequency.SECOND: 'S5', # The bar summarizes the trading activity during 1 second.
            bar.Frequency.MINUTE: 'M1', # The bar summarizes the trading activity during 1 minute.
            bar.Frequency.HOUR: 'H1', # The bar summarizes the trading activity during 1 hour.
            bar.Frequency.DAY: 'D', #The bar summarizes the trading activity during 1 day.
            bar.Frequency.WEEK: 'W', #The bar summarizes the trading activity during 1 week.
            bar.Frequency.MONTH: 'M' # The bar summarizes the trading activity during 1 month.
        }

def utcnow():
    return dt.as_utc(datetime.datetime.utcnow())


class PollingThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.__stopped = False

    def __wait(self):
        # Wait until getNextCallDateTime checking for cancelation every 0.5 second.
        nextCall = self.getNextCallDateTime()
        while not self.__stopped and utcnow() < nextCall:
            time.sleep(0.5)

    def stop(self):
        self.__stopped = True

    def stopped(self):
        return self.__stopped

    def run(self):
        logger.debug("Thread started.")
        while not self.__stopped:
            self.__wait()
            if not self.__stopped:
                try:
                    self.doCall()
                except Exception, e:
                    logger.error("Unhandled exception", exc_info=e)
        logger.debug("Thread finished.")

    # Must return a non-naive datetime.
    def getNextCallDateTime(self):
        raise NotImplementedError()

    def doCall(self):
        raise NotImplementedError()


def build_bar(barDict, identifier, frequency):
    # {u'complete': False, u'closeMid': 1.10334, u'highMid': 1.10387, u'lowMid': 1.10221, u'volume': 978, u'openMid': 1.10316, u'time': u'2015-07-06T11:00:00.000000Z'}
    ret = oandapy.parse_datetime(barDict['time'])
    startDateTime = dt.as_utc(ret)
  
    return bar.BasicBar(startDateTime, barDict["openMid"], barDict["highMid"], barDict["lowMid"], barDict["closeMid"], barDict["volume"], None, frequency)

class GetBarThread(PollingThread):

    # Events
    ON_BARS = 1

    def __init__(self, queue, oanda, identifiers, frequency, apiCallDelay=0):
        PollingThread.__init__(self)

        self.__period = OANDA_FREQUENCY[frequency]
        self.__oanda = oanda
        self.__queue = queue
        self.__identifiers = identifiers
        self.__frequency = frequency
        self.__nextBarClose = None
        self.__lastBarClose = {}
        # The delay between the bar's close and the API call.
        self.__apiCallDelay = apiCallDelay
        self.__updateNextBarClose(utcnow())

    def __updateNextBarClose(self,lastBar):
        self.__nextBarClose = resamplebase.build_range(lastBar, self.__frequency).getEnding()

    def getNextCallDateTime(self):
        return self.__nextBarClose + self.__apiCallDelay

    def doCall(self):
        self.__updateNextBarClose(self.__nextBarClose)
        barDict = {}

        for identifier in self.__identifiers:
            # See Oanda developer docs for more information
            # http://developer.oanda.com/rest-live/rates/#retrieveInstrumentHistory
            try:
                if identifier in self.__lastBarClose:
                    logger.debug("Requesting bars with %s period for %s, last call %s" % (self.__period, identifier,self.__lastBarClose[identifier]))
                    response = self.__oanda.get_history(
                        instrument=identifier,
                        start=self.__lastBarClose[identifier],
                        includeFirst='false',
                        granularity=self.__period,
                        candleFormat='midpoint')
                else:
                    logger.debug("Requesting bars with %s period for %s, first call" % (self.__period, identifier))
                    response = self.__oanda.get_history(
                        instrument=identifier,
                        count=2,
                        granularity=self.__period,
                        candleFormat='midpoint')

                logger.debug(response)
            except oandapy.OandaError, e:
                logger.error(e)
                response = {}

            if 'candles' in response:
                if len(response['candles']) > 0:
                    candle = response['candles'][0] # first candle is complete
                    self.__lastBarClose[identifier] = candle['time']                
                    barDict[identifier] = build_bar(candle, identifier, self.__frequency)

        if len(barDict):
            bars = bar.Bars(barDict)
            self.__queue.put((GetBarThread.ON_BARS, bars))

class LiveFeed(barfeed.BaseBarFeed):
    """A real-time BarFeed that builds bars using Oanda API.

    :param apiToken: The API token to authenticate calls to Oanda APIs.
    :type apiToken: string.
    :param identifiers: A list with the fully qualified identifier for the securities including the exchange suffix.
    :type identifiers: list.
    :param frequency: The frequency of the bars.
        Must be greater than or equal to **bar.Frequency.MINUTE** and less than **bar.Frequency.DAY**.
    :param apiCallDelay: The delay in seconds between the bar's close and the API call.
        This is necessary because the bar may not be immediately available.
    :type apiCallDelay: int.
    :param maxLen: The maximum number of values that the :class:`pyalgotrade.dataseries.bards.BarDataSeries` will hold.
        Once a bounded length is full, when new items are added, a corresponding number of items are discarded from the opposite end.
    :type maxLen: int.

    """

    QUEUE_TIMEOUT = 0.01

    def __init__(self, environment, apiToken, identifiers, frequency, apiCallDelay=1, maxLen=dataseries.DEFAULT_MAX_LEN):
        barfeed.BaseBarFeed.__init__(self, frequency, maxLen)
        if not isinstance(identifiers, list):
            raise Exception("identifiers must be a list")
        self.__oanda = oandapy.API(environment, apiToken)
        self.__queue = Queue.Queue()
        self.__thread = GetBarThread(self.__queue, self.__oanda, identifiers, frequency, datetime.timedelta(seconds=apiCallDelay))
        for instrument in identifiers:
            self.registerInstrument(instrument)

    ######################################################################
    # observer.Subject interface

    def start(self):
        if self.__thread.is_alive():
            raise Exception("Already strated")

        # Start the thread that runs the client.
        self.__thread.start()

    def stop(self):
        self.__thread.stop()

    def join(self):
        if self.__thread.is_alive():
            self.__thread.join()

    def eof(self):
        return self.__thread.stopped()

    def peekDateTime(self):
        return None

    ######################################################################
    # barfeed.BaseBarFeed interface

    def getCurrentDateTime(self):
        return utcnow()

    def barsHaveAdjClose(self):
        return False

    def getNextBars(self):
        ret = None
        try:
            eventType, eventData = self.__queue.get(True, LiveFeed.QUEUE_TIMEOUT)
            if eventType == GetBarThread.ON_BARS:
                ret = eventData
            else:
                logger.error("Invalid event received: %s - %s" % (eventType, eventData))
        except Queue.Empty:
            pass
        return ret

class Feed(csvfeed.GenericBarFeed):
    """A :class:`pyalgotrade.barfeed.csvfeed.BarFeed` that loads bars from CSV files downloaded from Oanda.

    :param frequency: The frequency of the bars. Only **pyalgotrade.bar.Frequency.DAY** or **pyalgotrade.bar.Frequency.WEEK**
        are supported.
    :param timezone: The default timezone to use to localize bars. Check :mod:`pyalgotrade.marketsession`.
    :type timezone: A pytz timezone.
    :param maxLen: The maximum number of values that the :class:`pyalgotrade.dataseries.bards.BarDataSeries` will hold.
        Once a bounded length is full, when new items are added, a corresponding number of items are discarded from the opposite end.
    :type maxLen: int.

    .. note::
        When working with multiple instruments:

            * If all the instruments loaded are in the same timezone, then the timezone parameter may not be specified.
            * If any of the instruments loaded are in different timezones, then the timezone parameter must be set.
    """

    def __init__(self, frequency=bar.Frequency.DAY, timezone=None, maxLen=dataseries.DEFAULT_MAX_LEN):
        if frequency not in OANDA_FREQUENCY:
            raise Exception("Frequency not supported by OANDA")

        csvfeed.GenericBarFeed.__init__(self, frequency, timezone, maxLen)

        self.setNoAdjClose()
        self.setDateTimeFormat("%Y-%m-%dT%H:%M:%S.%fZ")
        self.setColumnName("datetime", "time")
        self.setColumnName("open", "openMid")
        self.setColumnName("high", "highMid")
        self.setColumnName("low", "lowMid")
        self.setColumnName("close", "closeMid")
        self.setColumnName("volume", "volume")

class Oanda(oandapy.API):
    def __init__(self, environment, apiToken, accountId):
        oandapy.API.__init__(self,environment,apiToken)
        self._instruments = None
        self._accountId = accountId

    def download_bars(self,instrument, csvFile, start=None, end=None, days=90, frequency=bar.Frequency.MINUTE):
        if not end:
            end = utcnow() - datetime.timedelta(seconds=frequency)
        if not start:
            start = utcnow() - datetime.timedelta(days=days) - datetime.timedelta(seconds=frequency*2)

        logger.info('Downloading historic price data for instrument %s between %s and %s with frequency %s' % (instrument,start,end,frequency))
        candles = []        
        lastBarTime = end.isoformat()
        while lastBarTime > start.isoformat():
            response = self.get_history(
                            instrument=instrument,
                            end=lastBarTime,
                            count=OANDA_MAX_COUNT,
                            granularity=OANDA_FREQUENCY[frequency],
                            candleFormat='midpoint')
            lastBarTime = response['candles'][0]['time']
            filtered_candles = [c for c in response['candles'] if c['time'] > start.isoformat()]
            candles +=  filtered_candles[::-1] # reverse before adding
        
        candles.reverse()
        #print(candles)
        #print(candles[0]['time'],candles[-1]['time'])
        
        with open(csvFile, 'wb') as f:
            ##Date Time,Open,High,Low,Close,Volume,Adj Close
            #2013-01-01 13:59:00,13.51001,13.56,13.51,13.56,273.88014126,13.51001
            w = csv.DictWriter(f, ['time','openMid','highMid','lowMid','closeMid','volume'], extrasaction='ignore')
            w.writeheader()
            [w.writerow(candle) for candle in candles]
        logger.info('%d candles written to %s' % (len(candles),csvFile))
        return True

    def instruments(self,accountId=None, cache=False):
        if not accountId:
            accountId = self._accountId
        if not self._instruments:
            cache = True
        if cache:
            response = self.get_instruments(accountId)
            instruments = {}
            for instrument in response['instruments']:
                instruments[instrument['instrument']] = instrument
            self._instruments = instruments
        return self._instruments

if __name__ == '__main__':
    # DEBUG PURPOSES ONLY - please ignore
    logger.info('Starting')

    oanda = Oanda('practice','666b4dff976034e9b3e9102b31a5dc25-5266906d5d61bc6e3a0d817deee4b534','8638902')
    instruments = oanda.instruments() 
    assert len(instruments) == 122
    #instrument = instruments[instruments.keys()[0]]['instrument']
    instrument = 'GBP_USD'
    csvFile = '../data/'+instrument+'-oanda.csv'
    #assert oanda.download_bars(instrument,csvFile,days=1,frequency=bar.Frequency.HOUR)
    assert oanda.download_bars(instrument,csvFile)

    feed = Feed()
    feed.addBarsFromCSV(instrument,csvFile) 

    logger.info('Finished')
