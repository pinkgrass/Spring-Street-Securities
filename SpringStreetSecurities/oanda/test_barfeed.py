from pyalgotrade import strategy
from pyalgotrade.bar import Frequency
import barfeed
import broker

class MyStrategy(strategy.BaseStrategy):
    def __init__(self, feed, brk, instrument):
        strategy.BaseStrategy.__init__(self, feed, brk)
        self.__instrument = instrument

    def onBars(self, bars):
        bar = bars[self.__instrument]
        self.info(bar.getClose())

environment='practice'
access_token='666b4dff976034e9b3e9102b31a5dc25-5266906d5d61bc6e3a0d817deee4b534'
account='8638902'
instruments = ['EUR_USD']
apiCallDelay = 10
barFeed = barfeed.LiveFeed(environment, access_token, instruments, Frequency.MINUTE, apiCallDelay)

brk = broker.LiveBroker(environment, access_token, account)
# Evaluate the strategy with the feed's bars.
myStrategy = MyStrategy(barFeed, brk, instruments[0])
myStrategy.run()
