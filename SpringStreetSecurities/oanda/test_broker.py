#from pyalgotrade.bitstamp import barfeed # use local copy
#from pyalgotrade.bitstamp import broker # use local copy
import barfeed
import broker

from pyalgotrade.bar import Frequency
from pyalgotrade import strategy
from pyalgotrade.technical import ma
from pyalgotrade.technical import cross


class Strategy(strategy.BaseStrategy):
    def __init__(self, feed, brk, instrument):
        strategy.BaseStrategy.__init__(self, feed, brk)
        smaPeriod = 20
        self.__instrument = instrument
        self.__prices = feed[self.__instrument].getCloseDataSeries()
        self.__sma = ma.SMA(self.__prices, smaPeriod)
        self.__bid = None
        self.__ask = None
        self.__position = None
        self.__posSize = 0.10

        # Subscribe to order book update events to get bid/ask prices to trade.
        #feed.getOrderBookUpdateEvent().subscribe(self.__onOrderBookUpdate)

    def __onOrderBookUpdate(self, orderBookUpdate):
        bid = orderBookUpdate.getBidPrices()[0]
        ask = orderBookUpdate.getAskPrices()[0]

        if bid != self.__bid or ask != self.__ask:
            self.__bid = bid
            self.__ask = ask
            self.info("Order book updated. Best bid: %s. Best ask: %s" % (self.__bid, self.__ask))

    def onEnterOk(self, position):
        self.info("Position opened at %s" % (position.getEntryOrder().getExecutionInfo().getPrice()))

    def onEnterCanceled(self, position):
        self.info("Position entry cancelled")
        self.__position = None

    def onExitOk(self, position):
        self.__position = None
        self.info("Position closed at %s" % (position.getExitOrder().getExecutionInfo().getPrice()))

    def onExitCanceled(self, position):
        # If the exit was canceled, re-submit it.
        self.__position.exitLimit(self.__bid)

    def onBars(self, bars):
        bar = bars[self.__instrument]
        close = bar.getClose()
        high = bar.getHigh()
        low = bar.getLow()

        self.info("Price: %s. Volume: %s." % (bar.getClose(), bar.getVolume()))

        # If a position was not opened, check if we should enter a long position.
        if self.__position is None:
            if cross.cross_above(self.__prices, self.__sma) > 0:
                self.info("Entry signal. Buy at %s" % (low))
                self.__position = self.enterLongLimit(self.__instrument, low, self.__posSize, True)
        # Check if we have to close the position.
        elif not self.__position.exitActive() and cross.cross_below(self.__prices, self.__sma) > 0:
            self.info("Exit signal. Sell at %s" % (high))
            self.__position.exitLimit(high)


def main():
    environment='practice'
    access_token='666b4dff976034e9b3e9102b31a5dc25-5266906d5d61bc6e3a0d817deee4b534'
    account='8638902'
    instruments = ['GBP_USD']
    apiCallDelay = 0
    barFeed = barfeed.LiveFeed(environment, access_token, instruments, Frequency.MINUTE, apiCallDelay)
    brk = broker.LiveBroker(environment, access_token, account)
    strat = Strategy(barFeed, brk, instruments[0])

    strat.run()

if __name__ == "__main__":
    main()
