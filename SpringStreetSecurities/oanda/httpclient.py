# PyAlgoTrade
#
# Copyright 2011-2015 Gabriel Martin Becedillas Ruiz
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
.. moduleauthor:: Gabriel Martin Becedillas Ruiz <gabriel.becedillas@gmail.com>
"""

import time
import datetime
import hmac
import hashlib
import urllib
import urllib2
import json
import threading

import oandapy


from pyalgotrade.utils import dt
#from pyalgotrade.bitstamp import common
import common

def parse_datetime(dateTime):
    '''
    2014-02-11T16:22:07Z
    '''
    try:
        ret = datetime.datetime.strptime(dateTime, "%Y-%m-%dT%H:%M:%SZ")
    except ValueError:
        ret = datetime.datetime.strptime(dateTime, "%Y-%m-%dT%H:%M:%S.%fZ")
    return dt.as_utc(ret)

class Instrument(object):
    '''
    {u'pip': u'1.0', u'instrument': u'AU200_AUD', u'maxTradeUnits': 200, u'displayName': u'Australia 200'}
    '''
    def __init__(self, jsonDict):
        self.__jsonDict = jsonDict

    def getDict(self):
        return self.__jsonDict

    def getName(self):
        return self.__jsonDict['displayName']

    def getSymbol(self):
        return self.__jsonDict['instrument']

class AccountBalance(object):
    '''
    {u'accountCurrency': u'USD', u'marginUsed': 0, u'marginAvail': 5000.5062, u'unrealizedPl': 0, u'realizedPl': 0, u'marginRate': 0.1, u'accountName': u'USD', u'openTrades': 0, u'openOrders': 0, u'balance': 5000.5062, u'accountId': 8638902}
    '''
    def __init__(self, jsonDict):
        self.__jsonDict = jsonDict

    def getDict(self):
        return self.__jsonDict

    def getUSDAvailable(self):
        return float(self.__jsonDict['balance']) # might not be USD

    def getBTCAvailable(self):
        return float(self.__jsonDict["balance"]) # returns USD

class Order(object):
    '''
    {
      "id" : 175427639,
      "instrument" : "EUR_USD",
      "units" : 20,
      "side" : "buy",
      "type" : "marketIfTouched",
      "time" : "2014-02-11T16:22:07Z",
      "price" : 1,
      "takeProfit" : 0,
      "stopLoss" : 0,
      "expiry" : "2014-02-15T16:22:07Z",
      "upperBound" : 0,
      "lowerBound" : 0,
      "trailingStop" : 0
    }
    '''
    def __init__(self, jsonDict):
        if 'time' not in jsonDict:
            jsonDict['time'] = datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%dT%H:%M:%SZ")
        self.__jsonDict = jsonDict
        #print(self.__jsonDict)


    def getDict(self):
        return self.__jsonDict

    def getId(self):
        return int(self.__jsonDict["id"])

    def isBuy(self):
        return self.__jsonDict["side"] == 'buy'

    def isSell(self):
        return self.__jsonDict["side"] == 'sell'

    def getPrice(self):
        return float(self.__jsonDict["price"])

    def getAmount(self):
        return float(self.__jsonDict["units"])

    def getDateTime(self):
        return parse_datetime(self.__jsonDict["time"])

    def getInstrument(self):
        return self.__jsonDict["instrument"]


class UserTransaction(object):
    '''
    {
      "id" : 1170980,
      "accountId" : 12345,
      "time" : "2014-02-12T20:02:45Z",
      "type" : "TRADE_CLOSE",
      "instrument" : "EUR_USD",
      "units" : 1000,
      "side" : "sell",
      "price" : 1.35921,
      "pl" : 1.93,
      "interest" : 0,
      "accountBalance" : 1000026.0723,
      "tradeId" : 175427703 //Closed trade id
    }
    '''
    def __init__(self, jsonDict):
        self.__jsonDict = jsonDict

    def getDict(self):
        return self.__jsonDict

    def getBTC(self):
        return float(self.__jsonDict["units"])

    def getBTCUSD(self):
        return float(self.__jsonDict["price"]) #WTF?

    def getDateTime(self):
        return parse_datetime(self.__jsonDict["time"])

    def getFee(self):
        return float(self.__jsonDict["interest"]) #FEE=INTEREST?

    def getId(self):
        return int(self.__jsonDict["id"])

    def getOrderId(self):
        return int(self.__jsonDict["tradeId"])

    def getUSD(self):
        return float(self.__jsonDict['price'])


class HTTPClient(object):
    USER_AGENT = "PyAlgoTrade"
    REQUEST_TIMEOUT = 30
    ORDER_TIMEOUT = 60 #minutes

    class UserTransactionType:
        MARKET_TRADE = 2

    def __init__(self, environment, access_token, account):
        self.__oanda = oandapy.API(environment, access_token)
        self.__account = account

    def getInstruments(self):
        #http://developer.oanda.com/docs/v1/rates/#get-an-instrument-list
        jsonResponse = self.__oanda.get_instruments(self.__account)
        return [Instrument(json_instrument) for json_instrument in jsonResponse['instruments']]

    def getAccountBalance(self):
        # http://developer.oanda.com/rest-live/accounts/#getAccountInformation
        jsonResponse = self.__oanda.get_account(self.__account)
        return AccountBalance(jsonResponse)

    def getOpenOrders(self):
        # http://developer.oanda.com/rest-live/orders/#getOrdersForAnAccount
        jsonResponse = self.__oanda.get_orders(self.__account)
        return [Order(json_open_order) for json_open_order in jsonResponse['orders']]

    def cancelOrder(self, orderId):
        # http://developer.oanda.com/rest-live/orders/#closeOrder
        try:
            jsonResponse = self.__oanda.close_order(self.__account, orderId)
        except oandapy.OandaError, e:
            common.logger.error(e)

    def buyLimit(self, instrument, limitPrice, quantity, expiry=None):
        # http://developer.oanda.com/rest-live/orders/#createNewOrder
        price = round(float(limitPrice),5)
        units = 1 if float(quantity) < 1.0 else int(round(float(quantity)))
        if not expiry:
             expiry = datetime.datetime.now() + datetime.timedelta(minutes=self.ORDER_TIMEOUT)
        expiry = datetime.datetime.strftime(expiry, "%Y-%m-%dT%H:%M:%SZ")
        
        print(instrument,quantity,price,expiry)
        jsonResponse = self.__oanda.create_order(self.__account,
            instrument=instrument,
            units=units,
            side='buy',
            type='limit',
            price=price,
            expiry=expiry
            )
        return Order(jsonResponse['orderOpened'])

    def sellLimit(self, instrument, limitPrice, quantity, expiry=None):
        # http://developer.oanda.com/rest-live/orders/#createNewOrder
        price = round(float(limitPrice),5)
        units = 1 if float(quantity) < 1.0 else int(round(float(quantity)))
        if not expiry:
             expiry = datetime.datetime.now() + datetime.timedelta(minutes=self.ORDER_TIMEOUT)
        expiry = datetime.datetime.strftime(expiry, "%Y-%m-%dT%H:%M:%SZ")
        
        print(instrument,quantity,price,expiry)
        jsonResponse = self.__oanda.create_order(self.__account,
            instrument=instrument,
            units=units,
            side='sell',
            type='limit',
            price=price,
            expiry=expiry
            )
        return Order(jsonResponse['orderOpened'])

    def getUserTransactions(self, transactionType=None):
        # http://developer.oanda.com/docs/v1/trades/#get-a-list-of-open-trades
        jsonResponse = self.__oanda.get_transaction_history(self.__account)
        jsonResponse = jsonResponse['transactions']
        if transactionType is not None:
            jsonUserTransactions = filter(
                lambda jsonUserTransaction: jsonUserTransaction["type"] == transactionType, jsonResponse
            )
        else:
            jsonUserTransactions = jsonResponse
        transactions = []
        for transaction in jsonUserTransactions:
            transaction_detail = self.__oanda.get_transaction(self.__account,transaction['id'])
            transactions += [UserTransaction(transaction_detail)]
        return transactions

if __name__ == "__main__":
    httpclient = HTTPClient(environment='practice', access_token='666b4dff976034e9b3e9102b31a5dc25-5266906d5d61bc6e3a0d817deee4b534', account='8638902')
    instruments = httpclient.getInstruments()
    
    print(httpclient.getAccountBalance().getUSDAvailable())
    httpclient.buyLimit(instrument='EUR_USD', limitPrice=100.12, quantity=1, expiry=None)
    for order in httpclient.getOpenOrders():
        print(order.getDict())
        httpclient.cancelOrder(order.getId())

    for transaction in httpclient.getUserTransactions():
        print(transaction.getDict())

