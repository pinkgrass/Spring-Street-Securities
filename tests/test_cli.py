import pytest
from click.testing import CliRunner
from SpringStreetSecurities import cli


@pytest.fixture
def runner():
    return CliRunner()


def test_cli(runner):
    result = runner.invoke(cli.main)
    assert result.exit_code == 0
    assert not result.exception
    assert 'Usage: main [OPTIONS] COMMAND [ARGS]...' in result.output.strip()


def test_cli_with_marketdata_without_apikey(runner):
    result = runner.invoke(cli.main, ['marketdata'])
    assert result.exception
    assert result.exit_code == 2
    assert 'Error: Missing option "--apikey"' in result.output.strip()


def test_cli_with_marketdata_without_account(runner):
    result = runner.invoke(cli.main, ['marketdata', '--apikey=foo'])
    assert result.exception
    assert result.exit_code == 2
    assert 'Error: Missing option "--account"' in result.output.strip()


def test_cli_with_marketdata(runner):
    result = runner.invoke(cli.main, ['marketdata', '--apikey=foo', '--account=bah'])
    assert result.exit_code == 0
    assert not result.exception
    assert 'Market data download finished' in result.output.strip()
