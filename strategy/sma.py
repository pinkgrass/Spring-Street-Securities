# Spring Street Securities - Strategy Performance Analysis
#
#   Copyright 2015 Richard Crook
#
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import itertools

from pyalgotrade import strategy
from pyalgotrade.technical import ma
from pyalgotrade.technical import cross

class SMA(strategy.BaseStrategy):
    def __init__(self, feed, brk, instrument, smaPeriod=20):
        strategy.BaseStrategy.__init__(self, feed, brk)

        self.__instrument = instrument
        self.__prices = feed[self.__instrument].getCloseDataSeries()
        self.__sma = ma.SMA(self.__prices, smaPeriod)
        self.__bid = None
        self.__ask = None
        self.__position = None
        self.__posSize = 0.10

        # Subscribe to order book update events to get bid/ask prices to trade.
        #feed.getOrderBookUpdateEvent().subscribe(self.__onOrderBookUpdate)
    
    ''' Add Order Book updates to this strategy
    def __onOrderBookUpdate(self, orderBookUpdate):
        bid = orderBookUpdate.getBidPrices()[0]
        ask = orderBookUpdate.getAskPrices()[0]

        if bid != self.__bid or ask != self.__ask:
            self.__bid = bid
            self.__ask = ask
            self.info("Order book updated. Best bid: %s. Best ask: %s" % (self.__bid, self.__ask))
    '''
    @classmethod
    def parameters_generator(self,instruments):
        smaPeriod = range(5, 50)
        return itertools.product(instruments, smaPeriod)

    def onEnterOk(self, position):
        self.info("Position opened at %s" % (position.getEntryOrder().getExecutionInfo().getPrice()))

    def onEnterCanceled(self, position):
        self.info("Position entry cancelled")
        self.__position = None

    def onExitOk(self, position):
        self.__position = None
        self.info("Position closed at %s" % (position.getExitOrder().getExecutionInfo().getPrice()))

    def onExitCanceled(self, position):
        # If the exit was canceled, re-submit it.
        self.__position.exitLimit(self.__bid)

    def onBars(self, bars):
        bar = bars[self.__instrument]
        close = bar.getClose()
        high = bar.getHigh()
        low = bar.getLow()

        self.info("Price: %s. Volume: %s." % (bar.getClose(), bar.getVolume()))

        # If a position was not opened, check if we should enter a long position.
        if self.__position is None:
            if cross.cross_above(self.__prices, self.__sma) > 0:
                self.info("Entry signal. Buy at %s" % (low))
                self.__position = self.enterLongLimit(self.__instrument, low, self.__posSize, True)
        # Check if we have to close the position.
        elif not self.__position.exitActive() and cross.cross_below(self.__prices, self.__sma) > 0:
            self.info("Exit signal. Sell at %s" % (high))
            self.__position.exitLimit(high)
